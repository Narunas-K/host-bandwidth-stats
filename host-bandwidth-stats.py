## Version 1.0.0
## By Narunas Kapocius
## Last edited: 2019-05-01

import numpy as np
import matplotlib.pyplot as plt
import io
import time
from collections import deque

class CircularBuffer(deque):
    def __init__(self, size=0):
        super(CircularBuffer, self).__init__(maxlen=size)
    @property
    def average(self):  # TODO: Make type check for integer or floats
        return sum(self)/len(self)


def terminate():
    try:
        answer = input("Do you want to continue? Y/N: ")
        if answer == "N":
            print("Exiting program..")
            exit()
        elif answer == "Y":
            pid = input("Enter process ID (PID):")
        else:
            pid = terminate()
    except:
        exit()
    return pid


def readNetStatFile(pid):
    try:
        with open('/proc/' + pid + '/net/netstat','r') as f:
                for line in f:
                    if "IpExt:" in line and not( "InNoRoutes" in line):
                        lineVar = line.split(" ")
        downlinkBytes = int(lineVar[7])
        uplinkBytes = int(lineVar[8])
        newStart = False
    except:
        print("Process is closed.")
        pid = terminate()
        downlinkBytes = 0
        uplinkBytes = 0
        newStart = True
    return downlinkBytes, uplinkBytes, pid, newStart


def sleepSec(start):
    end = time.time()
    print (end -start)
    while(end-start) <1:
        end = time.time()


def figurePlot(downlink, uplink, timeAxis):
    try:
        plt.plot(timeAxis, list(downlink), label="Download speed")
        plt.plot(timeAxis, list(uplink), label = "Upload speed")
        plt.draw()
        plt.legend(loc='upper right')
        plt.ylabel("Bandwidth MB/s")
        plt.xlabel("time, s")
        plt.pause(0.000001)
        plt.clf()
    except:
        exit()


if __name__ == '__main__':
    pid = input ("Enter process ID (PID):")   
    size = int(input ("Enter Y axis lentgh (s): "))
    prevDownlinkBytes, prevUplinkBytes, pid, newStart = readNetStatFile(pid)
    cb = CircularBuffer(size)
    downloadSpeed = CircularBuffer(size)
    uploadSpeed = CircularBuffer(size)
    timePassed = 0
    start = time.time()
    while True:
        figurePlot(downloadSpeed, uploadSpeed, cb)
        sleepSec(start)
        start = time.time()
        downlinkBytes, uplinkBytes, pid, newStart = readNetStatFile(pid)
        if newStart == False:
            downlinkSpeed = ((downlinkBytes - prevDownlinkBytes))/1000000
            uplinkSpeed = ((uplinkBytes - prevUplinkBytes))/1000000
            print ("Download: " + str(downlinkSpeed) + "MB/s")
            print("Upload: " + str(uplinkSpeed) + "MB/s")
            prevDownlinkBytes = downlinkBytes 
            prevUplinkBytes = uplinkBytes
            timePassed = timePassed + 1
            cb.append(timePassed)
            downloadSpeed.append(downlinkSpeed)
            uploadSpeed.append(uplinkSpeed)
        else:
            prevDownlinkBytes, prevUplinkBytes, pid, newStart = readNetStatFile(pid)
            time.sleep(1)
            timePassed = 0
            cb = CircularBuffer(size)
